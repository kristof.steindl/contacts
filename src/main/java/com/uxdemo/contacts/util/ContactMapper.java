package com.uxdemo.contacts.util;

import com.uxdemo.contacts.dtos.ContactDTO;
import com.uxdemo.contacts.entities.Contact;

public class ContactMapper {
	
	public static ContactDTO convert(Contact contact) {
		ContactDTO contactDTO = new ContactDTO();
		
		// contact.get... methods some nullcheck is required 
		contactDTO.setId(contact.getId());
		contactDTO.setName(contact.getName());
		contactDTO.setEmail(contact.getEmail());
		contactDTO.setPhone(contact.getPhone());
		
		return contactDTO;
	}
	
	public static Contact convert(ContactDTO contactDTO) {
		Contact contact = new Contact();
		
		// contactDTO.get... methods some nullcheck is required 
		contact.setId(contactDTO.getId());
		contact.setName(contactDTO.getName());
		contact.setEmail(contactDTO.getEmail());
		contact.setPhone(contactDTO.getPhone());
		
		return contact;
	}

    
	
}
