package com.uxdemo.contacts.services;

import com.uxdemo.contacts.dtos.ContactDTO;
import com.uxdemo.contacts.entities.Contact;
import com.uxdemo.contacts.exceptions.GeneralContactException;
import com.uxdemo.contacts.repositories.ContactRepo;
import com.uxdemo.contacts.util.ContactMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Steindl Kristof
 */
@Service
public class ContactService {
	
	@Autowired
	ContactRepo contactRepo;

	public ContactService() {
	}
	
	public List<ContactDTO> findAll() throws GeneralContactException {
		List<Contact> contacts = contactRepo.findAll();
		if (contacts == null) {
			throw new GeneralContactException("unable fetch data from database");
		}
		if (contacts.size() == 0) {
			throw new GeneralContactException("no contact was found");
		}
		List<ContactDTO> contactDTOs = contacts.stream().map(contact -> ContactMapper.convert(contact)).collect(Collectors.toList());
		return contactDTOs;
	}
	
	public ContactDTO findById(Long id) throws GeneralContactException {
		Optional<Contact> contactOpt = contactRepo.findById(id);
		if (contactOpt.isPresent()) {
			ContactDTO contactDTO = ContactMapper.convert(contactOpt.get());
			return contactDTO;
		} else {
			throw new GeneralContactException("There is no contact with this id");
		}
	}
	
	public ContactDTO addContact(ContactDTO contactDTO) throws GeneralContactException {
		validateContactData(contactDTO);
		Contact contact = ContactMapper.convert(contactDTO);
		Contact persistedContact = contactRepo.saveAndFlush(contact);
		if (persistedContact.getId() == null) {
			throw new GeneralContactException("Unexpected error, contact wasn't stored");
		}
		return ContactMapper.convert(persistedContact);
	}
	
	public ContactDTO updateContact(ContactDTO contactDTO) throws GeneralContactException {
		// for id validation, with no id, exception is thrown;
		Long foundId = findById(contactDTO.getId()).getId();
		return addContact(contactDTO);
	}
	
	public Long deleteContact(Long id) throws GeneralContactException {
	// for id validation, with no id, exception is thrown;
		ContactDTO contactDTO = findById(id);
		contactRepo.delete(ContactMapper.convert(contactDTO));
		return contactDTO.getId();
	}
	
	private void validateContactData(ContactDTO contactDTO) throws GeneralContactException {
		if (contactDTO == null) {
			throw new GeneralContactException("No contact data");
		}
		StringBuilder stringBuilder = new StringBuilder();
		if (contactDTO.getName() == null || contactDTO.getName().equals("")) {
			stringBuilder.append("Name attribute must be filled\n");
		}
		if (contactDTO.getEmail()== null || contactDTO.getEmail().equals("")) {
			stringBuilder.append("Email attribute must be filled\n");
		}
		if (contactDTO.getPhone()== null || contactDTO.getPhone().equals("")) {
			stringBuilder.append("Phone attribute must be filled\n");
		}
		String errorMessage = stringBuilder.toString();
		if (!errorMessage.toString().equals("")) {
			throw new GeneralContactException(errorMessage);
		}
	}
	
}
