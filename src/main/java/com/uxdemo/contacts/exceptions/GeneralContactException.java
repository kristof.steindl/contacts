
package com.uxdemo.contacts.exceptions;

/**
 *
 * @author Steindl Kristof
 */
public class GeneralContactException extends RuntimeException{

	public GeneralContactException() {
	}

	public GeneralContactException(String message) {
		super(message);
	}
	
	
	
}
